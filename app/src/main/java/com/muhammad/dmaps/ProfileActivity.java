package com.muhammad.dmaps;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {
    private TextView textViewUsername, textViewEmail;
    private SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String PREF_NAME;
    Integer PRIVATE_MODE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        PREF_NAME = "DMAPS";
        PRIVATE_MODE = 0;
        sharedPreferences = this.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        textViewUsername = findViewById(R.id.text_view_username);
        textViewEmail = findViewById(R.id.text_view_email_profile);
        textViewUsername.setText(sharedPreferences.getString("namaUser","kosong"));
        textViewEmail.setText(sharedPreferences.getString("emailUser",""));
    }
}
