package com.muhammad.dmaps;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.muhammad.dmaps.utility.TrackGPS;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback   {

    private GoogleMap mMap;
    private FirebaseFirestore firebaseFirestore;
    private TrackGPS gps;
    CustomClusterRenderer renderer;

    private ClusterManager<StringClusterItem> mClusterManager;

        @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        final SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        }

        @Override public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        firebaseFirestore = FirebaseFirestore.getInstance();
        gps = new TrackGPS(this);
        mClusterManager = new ClusterManager<>(this, mMap);
        renderer = new CustomClusterRenderer(this, mMap, mClusterManager);
        mClusterManager.setRenderer(renderer);
        mClusterManager.setOnClusterClickListener(
                new ClusterManager.OnClusterClickListener<StringClusterItem>() {
                  @Override public boolean onClusterClick(Cluster<StringClusterItem> cluster) {
                    Toast.makeText(MapsActivity.this, "Cluster click", Toast.LENGTH_SHORT).show();
                    return false;
                  }
                });

       /* mClusterManager.setOnClusterItemClickListener(
                new ClusterManager.OnClusterItemClickListener<StringClusterItem>() {
                  @Override public boolean onClusterItemClick(StringClusterItem clusterItem) {
                    Toast.makeText(MapsActivity.this, "Cluster item click", Toast.LENGTH_SHORT).show();
                    // if true, click handling stops here and do not show info view, do not move camera
                    // you can avoid this by calling:
                    // renderer.getMarker(clusterItem).showInfoWindow()
                    return false;
                  }
                });*/

        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new CustomInfoViewAdapter(LayoutInflater.from(this)));
        mClusterManager.setOnClusterItemInfoWindowClickListener(
                new ClusterManager.OnClusterItemInfoWindowClickListener<StringClusterItem>() {
                  @Override public void onClusterItemInfoWindowClick(StringClusterItem stringClusterItem) {
                    Toast.makeText(MapsActivity.this, "Clicked info window: " + stringClusterItem.title,
                            Toast.LENGTH_SHORT).show();
                  }
                });

//        mMap.setOnInfoWindowClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mMap.setOnCameraIdleListener(mClusterManager);
//        mMap.setOnMarkerClickListener(mClusterManager);

        if (gps.canGetLocation()){
          LatLng myLatLng = new LatLng(gps.getLatitude(),gps.getLongitude());
          mMap.addMarker(new MarkerOptions()
                  .position(myLatLng));
          mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(gps.getLatitude(),gps.getLongitude()), 10));
        }
        addMarker();
        mClusterManager.cluster();
        }

        static class StringClusterItem implements ClusterItem {

        final String title;
        final LatLng latLng;

        public StringClusterItem(String title, LatLng latLng) {
          this.title = title;
          this.latLng = latLng;
        }

        @Override public LatLng getPosition() {
          return latLng;
        }

        @Override
        public String getTitle() {
          return null;
        }

        @Override
        public String getSnippet() {
          return null;
        }
        }

        public void addMarker() {
            Date date = new Date();
            final SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy hh:mm:ss a");
            final CharSequence current_date = DateFormat.format("d-MMM-yyyy hh:mm:ss a", date.getTime());
            Date newDate = subtractDays(date, 30);
            final CharSequence start_date = DateFormat.format("d-MMM-yyyy hh:mm:ss a", newDate.getTime());
            Date now = null;
            Date start = null;
            try {
                now = format.parse((String) current_date);
                start = format.parse((String) start_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            CollectionReference collectionReference =  firebaseFirestore.collection("reports");
            final Query query = firebaseFirestore.collection("reports")
                    .orderBy("reportTime").whereGreaterThanOrEqualTo("reportTime",start);

            System.out.println("Java Date after subtracting " + 30 + " days: " + newDate.toString());

                    query
                            .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot documentSnapshots) {
                            mClusterManager.cluster();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                    Double latitude = (Double) documentSnapshot.get("latitude");
                                    Double longitude = (Double) documentSnapshot.get("longitude");
                                    String judul = documentSnapshot.get("reportTitle").toString();
                                    LatLng latLng3 = new LatLng(latitude, longitude);
                                    try {

                                        Boolean test = (Boolean) documentSnapshot.get("isValid");
                                        if (test == true) {
                                            mClusterManager.addItem(new StringClusterItem(judul, latLng3));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                    }
                                }
                                mClusterManager.cluster();
                            }
                        }
                    });


        }

    public static Date subtractDays(Date date, int days) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, -days);

        return cal.getTime();
    }
}

