package com.muhammad.dmaps;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.muhammad.dmaps.fragments.ReportFragment;
import com.muhammad.dmaps.utility.TrackGPS;

public class MainActivity extends AppCompatActivity {

    private static final int NOTIFICATION_ID = 1;
    private TextView mTextMessage;
    private android.support.v4.app.Fragment fragment;
    private FragmentManager fragmentManager;
    private BottomNavigationView navigation;
    private Context context;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    TrackGPS gps;
    double longitude;
    double latitude;
    SharedPreferences sharedPreferences;
    String PREF_NAME = "DMAPS";
    int PRIVATE_MODE = 0;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_setting){
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        sharedPreferences = this.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();


        gps = new TrackGPS(this);
        if (gps.canGetLocation()) {
            Location locationNow = new Location("");
            Location locationReport = new Location("");
            longitude = gps.getLongitude();
            latitude = gps.getLatitude();
            locationNow.setLongitude(longitude);
            locationNow.setLatitude(latitude);
            editor.putFloat("longitude", (float) longitude);
            editor.putFloat("latitude", (float) latitude);
            editor.commit();
        }
    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigation =  (BottomNavigationView) findViewById(R.id.navigation);
        navigation.inflateMenu(R.menu.menu_bottom_navigation);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, new ReportFragment()).commit();

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id){
                    case R.id.item_report:
                        fragment = new ReportFragment();
                        break;
                    case R.id.item_map:
                        startActivity(new Intent(MainActivity.this, MapsActivity.class));
                        fragment = new ReportFragment();
                        break;
                }
                final FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.container, fragment).commit();
                return true;
            }
        });
    }
}
