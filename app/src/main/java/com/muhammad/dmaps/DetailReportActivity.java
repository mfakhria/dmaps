package com.muhammad.dmaps;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class DetailReportActivity extends AppCompatActivity {
    TextView  textReportDetail, textValid, textInvalid;
    ImageView reportImage;
    String urlReportImage, userId;
    Button btnValid;
    Button btnInvalid;
    FirebaseFirestore firebaseFirestore;
    SharedPreferences sharedPreferences;
    String PREF_NAME;
    Integer PRIVATE_MODE;
    Integer validCount;
    Integer invalidCount;
    Intent intent = getIntent();

    boolean voteValid;
    boolean voted;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textReportDetail = findViewById(R.id.report_detail_title);
        textInvalid = findViewById(R.id.text_invalid_count);
        textValid = findViewById(R.id.text_valid_count);
        reportImage = findViewById(R.id.image_view_report_detail);
        btnValid = findViewById(R.id.btn_valid);
        firebaseFirestore = FirebaseFirestore.getInstance();
        PREF_NAME = "DMAPS";
        PRIVATE_MODE = 0;
        sharedPreferences = this.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        final Intent intent = getIntent();

        voted = false;


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }
        });



        setAllData(intent);






        Toast.makeText(this, "valid count: "+String.valueOf(validCount), Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "invalid count: "+String.valueOf(invalidCount), Toast.LENGTH_SHORT).show();
        btnInvalid = findViewById(R.id.btn_invalid);

        RequestOptions options = new RequestOptions();
        options.centerCrop();


        Glide.with(this)
                .load(urlReportImage).apply(options)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        //  ((ReportViewHolder) holder).progressBar.setVisibility(View.INVISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).apply(options)
                .into(reportImage);




        firebaseFirestore.collection("reportVal")
        .document(intent.getStringExtra("reportId")+"_"+sharedPreferences.getString("userID",""))
                .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    voteValid = (boolean) documentSnapshot.get("valid");
                    if (voteValid) {
                        voteValid = true;
                        voted = true;
                        btnValid.setBackground(ContextCompat.getDrawable(DetailReportActivity.this, R.drawable.success_true));
                        btnValid.setBackgroundTintMode(null);
                    } else {
                        voteValid = false;
                        voted = true;
                        btnValid.setBackground(ContextCompat.getDrawable(DetailReportActivity.this, R.drawable.ic_success_false));
                        btnInvalid.setBackground(ContextCompat.getDrawable(DetailReportActivity.this, R.drawable.ic_error_true));
                        btnInvalid.setBackgroundTintMode(null);
                    }
                }else{
                    voted = false;
                }
            }
        });


        btnValid.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {

                clickValid(intent);


            }
        });

        btnInvalid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickInvalid(intent);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void clickValid(final Intent intent){
        if (voteValid){

        }else{
            btnValid.setBackground(ContextCompat.getDrawable(DetailReportActivity.this, R.drawable.success_true));
            btnInvalid.setBackground(ContextCompat.getDrawable(DetailReportActivity.this, R.drawable.ic_error));
            btnValid.setBackgroundTintMode(null);
            Toast.makeText(DetailReportActivity.this, "test", Toast.LENGTH_SHORT).show();
            final Map<String, Object> val = new HashMap<>();
            val.put("valid",true);
            final String reportId = intent.getStringExtra("reportId");


            firebaseFirestore.collection("reportVal")
                    .document(reportId+"_"+sharedPreferences.getString("userID",""))
                    .set(val)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Map<String, Object> validity = new HashMap<>();
                            validCount = validCount+1;
                            invalidCount = invalidCount;
                            textValid.setText(String.valueOf(validCount));
                            if (invalidCount > 0){
                                invalidCount = invalidCount-1;
                                textInvalid.setText(String.valueOf(invalidCount));
                            }
                            validity.put("validCount",validCount);
                            validity.put("invalidCount", invalidCount);
                            firebaseFirestore.collection("reports").document(intent.getStringExtra("reportId"))
                                    .update(validity).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    setValidity(validCount,invalidCount, reportId);
                                }
                            });
                        }
                    });

        }
    }

    public void clickInvalid(final Intent intent){
        Map<String, Object> val = new HashMap<>();
        btnInvalid.setBackground(ContextCompat.getDrawable(DetailReportActivity.this,R.drawable.ic_error_true));
        btnValid.setBackground(ContextCompat.getDrawable(DetailReportActivity.this,R.drawable.ic_success_false));
        final String reportId = intent.getStringExtra("reportId");
        val.put("valid",false);
        firebaseFirestore.collection("reportVal")
                .document(reportId+"_"+sharedPreferences.getString("userID",""))
                .set(val)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Map<String, Object> validity = new HashMap<>();
                        validCount = validCount;
                        invalidCount = invalidCount+1;
                        textInvalid.setText(String.valueOf(invalidCount));
                        if (validCount > 0){
                            validCount = validCount-1;
                            textValid.setText(String.valueOf(validCount));
                        }

                        validity.put("validCount",validCount);
                        validity.put("invalidCount",invalidCount);
                        firebaseFirestore.collection("reports").document(intent.getStringExtra("reportId"))
                                .update(validity).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                setValidity(validCount,invalidCount, reportId);
                            }
                        });
                    }
                });
    }

    public void setAllData(Intent intent){

        CollapsingToolbarLayout toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolbarLayout.setTitle(intent.getStringExtra("reportTitle"));

        textReportDetail.setText(intent.getStringExtra("reportDetail"));
        urlReportImage = intent.getStringExtra("reportImageUrl");
        validCount = intent.getIntExtra("validCount",0);
        invalidCount = intent.getIntExtra("invalidCount", 0);
        userId = intent.getStringExtra("userID");
        textValid.setText(String.valueOf(validCount));
        textInvalid.setText(String.valueOf(invalidCount));
    }

    public void setValidity(Integer validCount, Integer invalidCount, String reportId){
        Map<String, Object> isValid = new HashMap<>();
        if (validCount > invalidCount){
            isValid.put("isValid",true);
        }else{
            isValid.put("isValid",false);
        }
            firebaseFirestore.collection("reports").document(reportId)
                    .update(isValid).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            });

    }
}
