package com.muhammad.dmaps.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.muhammad.dmaps.R;

import com.muhammad.dmaps.ReportActivity;
import com.muhammad.dmaps.adapters.ReportListAdapter;
import com.muhammad.dmaps.models.ReportModel;

import java.util.ArrayList;
import java.util.List;


public class ReportFragment extends Fragment {
    static  String TAG = "ReportActivity";
    private List<ReportModel> reportList = new ArrayList<>();
    private ReportListAdapter adapter;
    private FloatingActionButton fab;//  private TextView pagetitle;
    private FirebaseFirestore firestoreDb;




    public ReportFragment() {
        // Required empty public constructor
    }

    /*public void readSetting(){
        pref = this.getActivity().getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        String phone1 = (pref.getString("Phone1", ""));

    }*/


    // TODO: Rename and change types and number of parameters
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);


    }

@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_report, container, false);
        rootView.findViewById(R.id.recycler_view_report);
        firestoreDb = FirebaseFirestore.getInstance();
        fab = rootView.findViewById(R.id.float_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), ReportActivity.class);
                startActivity(intent);
            }
        });

        final RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view_report);
        recyclerView.setHasFixedSize(true);
        adapter = new ReportListAdapter(getActivity(),reportList,getActivity().getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        getData(adapter);


        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        
    }

    public void getData(final ReportListAdapter adapter){
        firestoreDb.collection("reports")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot doc : task.getResult()) {
                                final ReportModel reportModel = new ReportModel();
                                final ReportModel serv_firebase = doc.toObject(ReportModel.class);
                                reportModel.setReportTitle(serv_firebase.getReportTitle());
                                reportModel.setReportDetail(serv_firebase.getReportDetail());
                                reportModel.setReportAddress(serv_firebase.getReportAddress());
                                reportModel.setLatitude(serv_firebase.getLatitude());
                                reportModel.setLongitude(serv_firebase.getLongitude());
                                reportModel.setImageUrl(serv_firebase.getImageUrl());
                                reportModel.setReportTime(serv_firebase.getReportTime());
                                reportModel.setJalan(serv_firebase.getJalan());
                                reportModel.setKecamatan(serv_firebase.getKecamatan());
                                reportModel.setKelurahan(serv_firebase.getKelurahan());
                                reportModel.setKota(serv_firebase.getKota());
                                reportModel.setReportImageUrl(serv_firebase.getReportImageUrl());
                                reportModel.setReportId(doc.getId());
                                reportModel.setReportHandled(serv_firebase.isReportHandled());
                                reportModel.setValidCount(serv_firebase.getValidCount());
                                reportModel.setInvalidCount(serv_firebase.getInvalidCount());
                                reportList.add(reportModel);
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

    }



}