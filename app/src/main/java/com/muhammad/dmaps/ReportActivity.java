package com.muhammad.dmaps;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.firebase.geofire.LocationCallback;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.muhammad.dmaps.utility.TrackGPS;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.ContentValues.TAG;


public class ReportActivity extends AppCompatActivity {
    private final int MY_CAMERA_REQUEST_CODE = 101;
    private final int REQUEST_IMAGE_CAPTURE = 111;
    private final int PROFILE_PHOTO = 112;
    private EditText reportTitle, reportDetail, reportHandler;
    private RadioGroup radioGroupHandle;
    private TextInputLayout textInputLayout;
    private Map<String, Object> reportMap;
    boolean reportIsReportHandled;
    private FusedLocationProviderClient client;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private Button btnGetLocation;
    private Geocoder geocoder;
    private TextView textLocation;
    private FloatingActionButton floatButtonReport;
    private ImageView imageViewReport;
    Uri mCropImageUri;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private TrackGPS gps;
    private Double longitude, latitude;
    private String provinsi;
    private String kota;
    private String kecamatan;
    private String kelurahan;
    private String jalan;
    private SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String PREF_NAME;
    Integer PRIVATE_MODE;
    StorageReference storageReference;
    FirebaseStorage storage;
    String reportImageUrl;
    FirebaseFirestore db;
    String reportAddress;
    EditText reportHandledBy;
    boolean imageOK;






    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        PREF_NAME = "DMAPS";
        PRIVATE_MODE = 0;
        sharedPreferences = this.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
        reportTitle = findViewById(R.id.report_title);
        reportDetail = findViewById(R.id.report_detail);
        reportHandler = findViewById(R.id.report_handled_by);
        textInputLayout = findViewById(R.id.layout_input_handler);
        radioGroupHandle = findViewById(R.id.group_handle);
        reportMap = new HashMap<String, Object>();
        client = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        btnGetLocation = findViewById(R.id.btn_get_location);
        floatButtonReport = findViewById(R.id.floating_button_report);
        imageViewReport = findViewById(R.id.report_image);
        reportHandledBy = findViewById(R.id.report_handled_by);
        displayLocationSettingsRequest(ReportActivity.this);
        requestPermission();
        textLocation = findViewById(R.id.text_location);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        db = FirebaseFirestore.getInstance();

        reportIsReportHandled=false;
        radioGroupHandle.check(R.id.no);
        geocoder = new Geocoder(getBaseContext(), Locale.forLanguageTag("INDONESIA"));
        btnGetLocation.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                requestPermission();
                displayLocationSettingsRequest(ReportActivity.this);
                getLocation();
            }
        });

        Button buttonUpload = findViewById(R.id.button_upload);

        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonUploadClick();
            }
        });

        radioGroupHandle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.yes) {
                    textInputLayout.setVisibility(View.VISIBLE);
                    reportIsReportHandled = true;
                }
                if (checkedId == R.id.no) {
                    textInputLayout.setVisibility(View.GONE);
                    reportHandler.getText().clear();
                    reportIsReportHandled = false;
                }
            }
        });

        floatButtonReport.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                onSelectImageClick(v);
                //checkPermissionCamera();
            }
        });
    }






    protected void getLocation() {
        if (ActivityCompat.checkSelfPermission(ReportActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }


        gps = new TrackGPS(ReportActivity.this);
        if (gps.canGetLocation()) {
            longitude = gps.getLongitude();
            latitude = gps.getLatitude();

            String strAdd = "";
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (addresses != null) {
                    Address returnedAddress = addresses.get(0);

                    StringBuilder strReturnedAddress = new StringBuilder("");

                    for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    }
                    strAdd = strReturnedAddress.toString();
                    reportAddress = strAdd;
                    textLocation.setText(strAdd);


                    if (addresses.size() > 0) {
                        String alamat = addresses.get(0).getAddressLine(0);
                        List<String> alamatList = Arrays.asList(alamat.split(", "));
                        Integer alamatListLength = alamatList.size();
                        if (alamatListLength == 6){
                            try {
                                jalan = (alamatList.get(alamatListLength - 6).replaceAll("No.*",""));
                                provinsi=(alamatList.get(alamatListLength - 2).replaceAll("\\d",""));
                                kota =(alamatList.get(alamatListLength - 3));
                                kecamatan = (alamatList.get(alamatListLength - 4));
                                kelurahan = (alamatList.get(alamatListLength - 5));
                            } catch (Error e) {
                                e.printStackTrace();
                            }
                        }else
                        if (alamatListLength == 5){
                            jalan = ("");
                            provinsi = ( alamatList.get(alamatListLength - 3).replaceAll("\\d",""));
                            kota = (alamatList.get(alamatListLength - 4));
                            kecamatan = (alamatList.get(alamatListLength - 4));
                            //kelurahan.setText(addresses.get(0).getAddressLine(0));
                            kelurahan = (alamatList.get(alamatListLength - 5));
                            //ETuserAddress.setText(addresses.get(0).getAddressLine(0));

                        }else
                        if (alamatListLength == 4){
                            jalan = ("");
                            provinsi = ( alamatList.get(alamatListLength - 2).replaceAll("\\d",""));
                            kota = (alamatList.get(alamatListLength - 3));
                            kecamatan = (alamatList.get(alamatListLength - 4));
                            kelurahan = (addresses.get(0).getAddressLine(0));
                            //ETuserAddress.setText(addresses.get(0).getAddressLine(0));


                        }else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(ReportActivity.this);
                            builder.setMessage("Cari alamat menggunakan nama jalan")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //do things
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                        System.out.println(addresses.get(0).getCountryName());
                    }
                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }





    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1);
    }





    private void displayLocationSettingsRequest(final Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context).addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                        try {
                            status.startResolutionForResult(ReportActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }







    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkPermissionCamera() {
        if (checkSelfPermission(android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                    MY_CAMERA_REQUEST_CODE);


        } else {
            CropImage.startPickImageActivity(this);
        }
    }









    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CropImage.startPickImageActivity(this);
                } else {

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


///////////////////////



    public void onSelectImageClick(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissionCamera();
        }else {
            CropImage.startPickImageActivity(this);
            //CropImage.getCameraIntent(this,mCropImageUri);
        }
    }



    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                //        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri, this);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                ((ImageView) findViewById(R.id.report_image)).setImageURI(result.getUri());
                //       Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }




    }

    public void startCropImageActivity(Uri imageUri, Activity activity) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setInitialCropWindowPaddingRatio((float) 0)
                .setFixAspectRatio(true)
                .start(activity);

        imageOK = true;
    }

    public void uploadData(){
        Date d = new Date();
        final SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy hh:mm:ss a");
        final CharSequence current_date = DateFormat.format("d-MMM-yyyy hh:mm:ss a", d.getTime());
        imageViewReport.setDrawingCacheEnabled(true);
        imageViewReport.buildDrawingCache();
        Bitmap bitmap = imageViewReport.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();
        final UploadTask uploadTask = storageReference.child("images/reports/"+ UUID.randomUUID()).putBytes(data);
        uploadTask.addOnSuccessListener(
                new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Date now = null;
                        try {
                            now = format.parse((String) current_date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        reportImageUrl = (taskSnapshot.getDownloadUrl()).toString();
                        reportMap.put("reportTime", now);
                        reportMap.put("reportTitle", reportTitle.getText().toString());
                        reportMap.put("reportDetail", reportDetail.getText().toString());
                        reportMap.put("userID", sharedPreferences.getString("userID",""));
                        if (longitude.equals("")||latitude.equals("")){
                            Toast.makeText(ReportActivity.this, "Lokasi kejadian masih kosong", Toast.LENGTH_LONG).show();
                        }else{
                            reportMap.put("longitude", longitude);
                            reportMap.put("latitude", latitude);
                            reportMap.put("jalan", jalan);
                            reportMap.put("kecamatan", kecamatan);
                            reportMap.put("kelurahan", kelurahan);
                            reportMap.put("kota", kota);
                            reportMap.put("provinsi", provinsi);
                            reportMap.put("reportImageUrl",reportImageUrl);
                            reportMap.put("reportAddress", reportAddress);
                            reportMap.put("reportValid", 0);
                            reportMap.put("reportInvalid", 0);
                            reportMap.put("reportHandled", reportIsReportHandled);
                            reportMap.put("reportHandledBy", reportHandledBy.getText().toString());
                            reportMap.put("validCount", 0);
                            reportMap.put("invalidCount", 0);
                            db.collection("reports")
                                    .add(reportMap)
                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(DocumentReference documentReference) {
                                            Toast.makeText(ReportActivity.this, "Berhasil menyimpan data", Toast.LENGTH_SHORT).show();
                                            Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                            Intent intent = new Intent(ReportActivity.this, MainActivity.class);
                                            startActivityForResult(intent,1);
                                            finish();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w(TAG, "Error adding document", e);
                                        }
                                    });
                        }

                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

            }
        }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

            }
        });
    }

    public void buttonUploadClick(){
        if(reportTitle.getText().equals("") || reportDetail.getText().equals("") || textLocation.getText().equals("") || imageOK == false){
            Toast.makeText(this, "Mohon isi semua data", Toast.LENGTH_SHORT).show();
        }else {
            uploadData();
        }

    }


}
