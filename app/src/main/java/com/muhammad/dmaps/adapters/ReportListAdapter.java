package com.muhammad.dmaps.adapters;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.auth.FirebaseAuth;
import com.muhammad.dmaps.R;
import com.muhammad.dmaps.DetailReportActivity;
import com.muhammad.dmaps.models.ReportModel;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ReportListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity activity;
    private List<ReportModel> reportItems;
    private Context mContext;
    private ReportListAdapter.OnItemClickListener mListener;

    private SharedPreferences sharedPreferences;
    String PREF_NAME = "DMAPS";
    int PRIVATE_MODE = 0;

    public interface OnItemClickListener{
        void onItemclick(int position);
    }

    public void setOnItemClickListener(ReportListAdapter.OnItemClickListener listener) {
        mListener = listener;
    }

    private class ReportViewHolder extends RecyclerView.ViewHolder {
        TextView   username,reportTime,reportDetail,title,reportDistance,textViewValid,textViewInvalid;
        ImageView imageViewReport;
        public ImageButton moremenu;
        ProgressBar progressBar;
        CardView cardViewReport;
        ReportViewHolder(View view, final ReportListAdapter.OnItemClickListener listener, int viewType) {
            super(view);
            title = (TextView) view.findViewById(R.id.text_report_title);
            reportDetail = view.findViewById(R.id.text_report_content);
            reportTime = view.findViewById(R.id.text_time_ago);
            reportDistance = view.findViewById(R.id.text_distance);
            imageViewReport = view.findViewById(R.id.image_view_report);
            cardViewReport = view.findViewById(R.id.card_view);
            textViewInvalid = view.findViewById(R.id.text_view_invalid);
            textViewValid = view.findViewById(R.id.text_view_valid);
        }
    }

    public ReportListAdapter(FragmentActivity activity, List<ReportModel> reportItems, Context context) {
        this.reportItems = reportItems;
        this.activity = activity;
        mContext = context;
    }

    public ReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View reportView = inflater.inflate(R.layout.row_list_report, parent, false);
        return new ReportListAdapter.ReportViewHolder(reportView,mListener, viewType);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        final ReportModel reports = reportItems.get(position);
        ((ReportViewHolder) holder).title.setText(reports.getReportTitle());
        ((ReportViewHolder) holder).reportDetail.setText(reports.getReportDetail());
        ((ReportViewHolder) holder).textViewValid.setText(String.valueOf(reports.getValidCount()));
        ((ReportViewHolder) holder).textViewInvalid.setText(String.valueOf(reports.getInvalidCount()));
        String date = String.valueOf(reports.getReportTime());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm z");
        Date testDate = reports.getReportTime();
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        System.out.println(testDate);

        sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        date = sdf.format(testDate);
        ((ReportViewHolder) holder).reportTime.setText(date);



        Glide.with(mContext)
                .load(reports.getReportImageUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        //  ((ReportViewHolder) holder).progressBar.setVisibility(View.INVISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).into(((ReportViewHolder) holder).imageViewReport);

        Double longitude;
        Double latitude;
        final float distance;

        if (ActivityCompat.checkSelfPermission(mContext, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        sharedPreferences = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        Location locationNow = new Location("");
        Location locationReport = new Location("");
        longitude = Double.valueOf(sharedPreferences.getFloat("longitude",0));
        latitude = Double.valueOf(sharedPreferences.getFloat("latitude",0));
        locationNow.setLongitude(longitude);
        locationNow.setLatitude(latitude);
        locationReport.setLongitude(reports.getLongitude());
        locationReport.setLatitude(reports.getLatitude());
        distance = locationNow.distanceTo(locationReport);
        ((ReportViewHolder) holder).reportDistance.setText(new DecimalFormat("##.##").format(distance) + "m dari lokasi saat ini");
        final String finalDate = date;
        ((ReportViewHolder) holder).cardViewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailReportActivity.class);
                intent.putExtra("reportTitle", reports.getReportTitle());
                intent.putExtra("reportDetail", reports.getReportDetail());
                intent.putExtra("reportTime", finalDate);
                intent.putExtra("reportDistance", String.valueOf(distance));
                intent.putExtra("reportId", reports.getReportId());
                intent.putExtra("reportImageUrl", reports.getReportImageUrl());
                intent.putExtra("validCount", reports.getValidCount());
                intent.putExtra("invalidCount", reports.getInvalidCount());
                Toast.makeText(activity, "invalidCount: "+String.valueOf(reports.getInvalidCount())
                        , Toast.LENGTH_SHORT).show();
                mContext.startActivity(intent);
            }
        });

        FrameLayout framelayout = new FrameLayout(mContext);
        framelayout.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,
                RecyclerView.LayoutParams.MATCH_PARENT));
    }

    @Override
    public int getItemCount () {
        return reportItems.size();
    }

}