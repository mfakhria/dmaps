package com.muhammad.dmaps.models;

import java.util.Date;

public class ReportModel {
    private String reportTitle;
    private String reportDetail;
    private Date reportTime;
    private String imageUrl;
    private String reportImageUrl;
    private String reportAddress;
    private String kelurahan;
    private String kecamatan;
    private String jalan;
    private String provinsi;
    private String kota;
    private Double Longitude;
    private Double Latitude;
    private boolean reportHandled;
    private Integer validCount;
    private Integer invalidCount;

    public ReportModel() {
    }

    /*public ReportModel(String reportTitle,
                       String reportDetail,
                       Date reportTime,
                       String imageUrl,
                       String reportImageUrl,
                       String reportAddress,
                       String kelurahan,
                       String kecamatan,
                       String jalan,
                       String provinsi,
                       String kota,
                       String reportId,
                       Integer validCount,
                       boolean reportHandled){
        this.reportTitle = reportTitle;
        this.reportDetail = reportDetail;
        this.reportTime = reportTime;
        this.imageUrl = imageUrl;
        this.reportImageUrl = reportImageUrl;
        this.reportAddress = reportAddress;
        this.kelurahan = kelurahan;
        this.kecamatan = kecamatan;
        this.jalan = jalan;
        this.provinsi = provinsi;
        this.kota = kota;
        this.reportHandled = reportHandled;
        this.reportId = reportId;
        this.validCount = validCount;
    }*/

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public String getReportDetail() {
        return reportDetail;
    }

    public void setReportDetail(String reportDetail) {
        this.reportDetail = reportDetail;
    }

    public Date getReportTime() {
        return reportTime;
    }

    public void setReportTime(Date reportTime) {
        this.reportTime = reportTime;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getReportImageUrl() {
        return reportImageUrl;
    }

    public void setReportImageUrl(String reportImageUrl) {
        this.reportImageUrl = reportImageUrl;
    }

    public String getReportAddress() {
        return reportAddress;
    }

    public void setReportAddress(String reportAddress) {
        this.reportAddress = reportAddress;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getJalan() {
        return jalan;
    }

    public void setJalan(String jalan) {
        this.jalan = jalan;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    private String reportId;

    public boolean isReportHandled() {
        return reportHandled;
    }

    public void setReportHandled(boolean reportHandled) {
        this.reportHandled = reportHandled;
    }

    public Integer getValidCount() {
        return validCount;
    }

    public void setValidCount(Integer validCount) {
        this.validCount = validCount;
    }

    public Integer getInvalidCount() {
        return invalidCount;
    }

    public void setInvalidCount(Integer invalidCount) {
        this.invalidCount = invalidCount;
    }

}
